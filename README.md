Little Wagon Photography is Ottawa’s leading newborn, maternity, baby, children and family Photographer. We offer professional and fine art photography in the Ottawa and surrounding area and provide all of our clients high resolution digital files with every session.

Address: 274 Par-La-Ville Cir, Stittsville, Ontario K2S 0M4, CANADA

Phone: 613-266-8934
